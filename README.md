CMS Tracker DAQ MW HERD Exploration
==

Usage
--
```
git submodule init
git submodule update
cd daq
docker build -t ph2acf_centos .
docker run -it --entrypoint <executable> ph2acf_centos
```
